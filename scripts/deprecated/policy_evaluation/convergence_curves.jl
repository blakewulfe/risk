
using PGFPlots
using TikzPictures

push!(LOAD_PATH, ".")
include("generate_risk_dataset.jl")

function collect_targets(flags)

    rng = MersenneTwister(flags["random_seed"])
    # set seed globally
    srand(flags["random_seed"])

    # compute total roadway length and add to flags
    total_length = (flags["roadway_length"] * 2 
        + 2 * pi * flags["roadway_radius"])
    add_entry!(flags, "total_roadway_length", total_length, Float64)

    # roadway
    roadway = gen_stadium_roadway(flags["num_lanes"], 
        length = flags["roadway_length"],
        radius = flags["roadway_radius"])
    
    # dataset containers, values
    total_time = flags["sampling_time"] + flags["sim_burn_in_time"] + 2 / flags["sampling_freq"]
    num_frames_per_episode = Int(total_time * flags["sampling_freq"]) 
    num_states_total = Int(
        num_frames_per_episode * flags["max_num_vehicles"])

    # objects used during simulation
    scene = Scene(flags["max_num_vehicles"])
    context = IntegratedContinuous(1 / flags["sampling_freq"], 1)
    models = Dict{Int, DriverModel}()

    # dataset containers
    targets = Vector{Float64}(flags["num_targets"])
    aggregate_targets = Array{Float64}(flags["num_targets"], 
        flags["num_monte_carlo_runs"], flags["num_traj"])
    rec = SceneRecord(num_frames_per_episode, 
        1. / flags["sampling_freq"], 
        flags["max_num_vehicles"])

    # traj data to use for storing information about an episode
    trajdata = Trajdata(roadway,
        Dict{Int, VehicleDef}(),
        Array{TrajdataState}(num_states_total),
        Array{TrajdataFrame}(num_frames_per_episode))

    # repeatedly simulate, collecting dataset
    for tidx in 1:flags["num_traj"]
        # generate a random initial scene
        reset!(scene, context, models, roadway, rng, flags)

        # initialize trajdata
        for veh in scene
            trajdata.vehdefs[veh.def.id] = veh.def
        end

        # simulate the scene for an initial period
        frame_idx = simulate_scene!(scene, context, models, 
            roadway, trajdata, rng, flags["sim_burn_in_time"])

        # repeatedly simulate the scene starting from frame_idx
        for midx in 1:flags["num_monte_carlo_runs"]
            get!(scene, trajdata, frame_idx)
            simulate_scene!(scene, context, models, roadway, 
                trajdata, rng, flags["sampling_time"], frame_idx)
            # extract target starting at first frame after burn in
            extract_risk_targets!(
                targets, rec, scene, trajdata, frame_idx + 1)
            aggregate_targets[:, midx, tidx] = targets
        end
    end
    return aggregate_targets
end

function compute_convergence_curves(targets)
    means = zeros(size(targets, 1))
    diff_sums = ones(size(targets, 1))
    mean_curve = Array{Float64}(size(targets))
    std_curve = Array{Float64}(size(targets))
    for idx in 1:size(targets, 2)
        temp_means = deepcopy(means)
        diff = targets[:,idx] - temp_means
        means += diff / idx
        new_diff = targets[:,idx] - means
        diff_sums += diff .* new_diff
        stds = sqrt(diff_sums / max(idx - 2, 1))
        mean_curve[:, idx] = means
        std_curve[:, idx] = stds
    end
    return mean_curve, std_curve
end

function plot_convergence_curve(curve, dir, name, ext = "pdf")
    a = Axis(Plots.Linear(collect(1:length(curve)), curve[1:end], markSize=.1), 
        xlabel="Simulations", ylabel="$(name)", title="Simulation vs $(name)")
    TikzPictures.save(string(dir, "cc_$(name).$(ext)"), a)
end

parse_flags!(FLAGS, ARGS)
FLAGS["num_traj"] = 100
FLAGS["sampling_time"] = 20.
FLAGS["num_monte_carlo_runs"] = 100
targets = collect_targets(FLAGS)
# fake_targets = reshape(randn(1000) * 50, 2, 500)
for idx in 1:FLAGS["num_traj"]
    num_collisions = sum(targets[1,:,idx])
    println(num_collisions)
    if num_collisions > 0.
        mean_curve, std_curve = compute_convergence_curves(targets[:,:,idx])
        plot_convergence_curve(
            mean_curve[1,:], "/Users/wulfebw/Desktop/", "$(idx) Collision Mean")
        plot_convergence_curve(
            mean_curve[2,:], "/Users/wulfebw/Desktop/", "$(idx) Hard Brake Mean")
        plot_convergence_curve(
            std_curve[1,:], "/Users/wulfebw/Desktop/", "$(idx) Collision Std")
        plot_convergence_curve(
            std_curve[2,:], "/Users/wulfebw/Desktop/", "$(idx) Hard Brake Std")
    end 
end
