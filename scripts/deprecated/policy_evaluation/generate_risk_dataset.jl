
FLAGS = Flags()

# scene constants
# roadway
add_entry!(FLAGS, 
    "num_lanes", 5, Int64, 
    "Number of lanes in the simulations.")
add_entry!(FLAGS, 
    "roadway_length", 200., Float64, 
    "Length of the roadway.")
add_entry!(FLAGS, 
    "roadway_radius", 50., Float64, 
    "Radius of turns in the roadway.")
add_entry!(FLAGS, 
    "lane_width", 3., Float64, 
    "Width of lane in meters.")

# vehicles
add_entry!(FLAGS, 
    "max_num_vehicles", 30, Int64, 
    "Number of vehicles on the road.")
add_entry!(FLAGS, 
    "base_speed", 20., Float64, 
    "Base vehicle speed.")
add_entry!(FLAGS, 
    "base_speed_variance", 2., Float64, 
    "Variance factor in inital speed.")
add_entry!(FLAGS, 
    "desired_speed", 30., Float64, 
    "Desired vehicle speed.")
add_entry!(FLAGS, 
    "desired_speed_variance", 3., Float64, 
    "Variance factor in desired speed.")
add_entry!(FLAGS, 
    "vehicle_length", 5., Float64, 
    "Vehicle length.")
add_entry!(FLAGS, 
    "vehicle_width", 2., Float64, 
    "Vehicle width.")
add_entry!(FLAGS, 
    "min_init_dist", 10., Float64, 
    "Minimum distance between vehicles at start of simulation.")
add_entry!(FLAGS, 
    "init_dist_variance", 1., Float64, 
    "Base variance in initial distance of vehicles.")

# simulation constants
add_entry!(FLAGS, 
    "num_traj", 100, Int64, 
    "Number of unique trajectories.")
add_entry!(FLAGS, 
    "num_monte_carlo_runs", 40, Int64, 
    "Number of monte carlo runs per trajectory.")
add_entry!(FLAGS, 
    "sampling_freq", 10, Int64, 
    "Number of samples per second (i.e., hz).")
add_entry!(FLAGS, 
    "sampling_time", 2., Float64, 
    "Seconds to simulate trajectory.")
add_entry!(FLAGS, 
    "sim_burn_in_time", 10., Float64, 
    "Time before collecting target values.")
add_entry!(FLAGS, 
    "num_proc", 1, Int64, 
    "Number of parallel processes for gathering dataset.")
add_entry!(FLAGS, 
    "verbose", 1, Int64, 
    "Level of verbosity in outputting information.")
add_entry!(FLAGS, 
    "random_seed", 1, Int64, 
    "Random seed value.")

# NGSIM constants
add_entry!(FLAGS, 
    "num_frames_threshold", 80, Int64, 
    "Number of frames a vehicle must be in to be sampled (10 is 1 sec).")
add_entry!(FLAGS, 
    "NGSIM_i101_set", 1, Int64, 
    "Which NGSIM time period to use {1,2,3}.")
add_entry!(FLAGS, 
    "NGSIM_burn_in_time", 1., Float64, 
    "Time before collecting target values.")
add_entry!(FLAGS, 
    "NGSIM_max_num_vehicles", 193, Int64, 
    "Number of vehicles on the road.")
add_entry!(FLAGS, 
    "NGSIM_num_traj", 1000, Int64, 
    "Number of vehicles on the road.")

# dataset constants
add_entry!(FLAGS, 
    "dataset_type", "heuristic", String, 
    "Type of dataset to generate.")
add_entry!(FLAGS, 
    "num_features", 57, Int64, 
    "Number of features (e.g., dist to car in front).")
add_entry!(FLAGS, 
    "num_targets", 2, Int64, 
    "Number of target values (e.g., p(collision).")
add_entry!(FLAGS, 
    "output_filepath", "../../data/datasets/risk.h5", String, 
    "Filepath where to save dataset.")
add_entry!(FLAGS, 
    "save_every", 100, Int64, 
    "Monte carlo simulations between checkpointing dataset.")

#=
Description:
    - Generate a risk dataset by running set of simulations
        and collecting (feature, target) values from those simulations.
        This method is used to generate risk datasets from both
        heuristic scenes and scenes derived from real data.

Args:
    - flags: Flags object with (possibly parsed) options.
=#
function generate_risk_dataset(flags)

    # random seed
    rng = MersenneTwister(flags["random_seed"])

    # set seed globally
    srand(flags["random_seed"])

    # set dataset_type specific values
    if flags["dataset_type"] == "heuristic"
        # compute total roadway length and add to flags
        total_length = (flags["roadway_length"] * 2 
            + 2 * pi * flags["roadway_radius"])
        add_entry!(flags, "total_roadway_length", total_length, Float64)

        # roadway
        roadway = gen_stadium_roadway(flags["num_lanes"], 
            length = flags["roadway_length"],
            radius = flags["roadway_radius"])
    elseif flags["dataset_type"] == "NGSIM"
        roadway = ROADWAY_101
        ngsim_trajdata = load_trajdata(flags["NGSIM_i101_set"])
    end
    
    # dataset size values
    total_time = flags["sampling_time"] + flags["sim_burn_in_time"] + 2 / flags["sampling_freq"]
    num_frames_per_episode = Int(total_time * flags["sampling_freq"]) 
    num_states_total = Int(
        num_frames_per_episode * flags["max_num_vehicles"])

    # objects used during simulation
    scene = Scene(flags["max_num_vehicles"])
    context = IntegratedContinuous(1 / flags["sampling_freq"], 1)
    models = Dict{Int, DriverModel}()

    # create file to write dataset to
    h5file = h5open(flags["output_filepath"], "w")
    feature_set = d_create(h5file, "features", datatype(Float64), 
        dataspace(flags["num_features"], flags["num_traj"]))
    target_set = d_create(h5file, "targets", datatype(Float64), 
        dataspace(flags["num_targets"], flags["num_traj"]))

    # dataset containers
    features = Vector{Float64}(flags["num_features"])
    targets = Vector{Float64}(flags["num_targets"])
    aggregate_targets = Vector{Float64}(flags["num_targets"])
    rec = SceneRecord(num_frames_per_episode, 
        1. / flags["sampling_freq"], 
        flags["max_num_vehicles"])

    # traj data to use for storing information about an episode
    trajdata = Trajdata(roadway,
        Dict{Int, VehicleDef}(),
        Array{TrajdataState}(num_states_total),
        Array{TrajdataFrame}(num_frames_per_episode))

    # repeatedly simulate, collecting dataset
    last_save_idx = 1
    for idx in 1:flags["num_traj"]
        # generate a random initial scene
        if flags["dataset_type"] == "heuristic"
            ego_veh_id = 1
            reset!(scene, context, models, roadway, rng, flags)
        elseif flags["dataset_type"] == "NGSIM"
            ego_veh_id = flags["veh_ids"][idx]
            reset!(scene, context, models, ngsim_trajdata, 
                ego_veh_id, rng, flags)
        end

        # run monte carlo simulations of scene
        # collecting features and target values
        monte_carlo!(scene, context, models, roadway, trajdata, ego_veh_id,
            features, targets, aggregate_targets, rec, rng, flags)

        # set dataset values
        feature_set[:, idx] = features
        target_set[:, idx] = aggregate_targets

        if idx % flags["save_every"] == 0 && flags["verbose"] > 0
           println("proc $(flags["random_seed"]) at run $(idx))")
        end
    end

    close(h5file)
    return true
end

#=
Description:
    - Run monte carlo simulations across multiple processes,
        collecting the feature_sets and target_sets from each,
        and then saving the entire dataset to file

Args:
    - flags: parsed command line options and constants
=#
function parallel_generate_heuristic_dataset(flags)

    # set seed globally for AutomotiveDrivingModels
    srand(flags["random_seed"])

    # compute number of trajectories for each process
    @assert flags["num_traj"] % flags["num_proc"] == 0
    num_proc_trajectories = Int(
        flags["num_traj"] / flags["num_proc"])

    # set save every if greater than total trajectories
    if flags["save_every"] > num_proc_trajectories
        flags["save_every"] = num_proc_trajectories
    end        

    # spawn the individual processes, each of which 
    # write to their own hdf5 file
    dir = dirname(flags["output_filepath"])
    filename = basename(flags["output_filepath"])
    dataset_filepaths::Vector{String} = []
    futures::Vector{Future} = []
    for i in 1:flags["num_proc"]

        # set flag values specific to each proc
        output_filepath = string(
            dir, "/proc_$(i)_$(filename)")
        push!(dataset_filepaths, output_filepath)
        proc_flags = deepcopy(flags)
        proc_flags["random_seed"] = i
        proc_flags["output_filepath"] = output_filepath
        proc_flags["num_traj"] = num_proc_trajectories

        # spawn the process and maintain its future
        f = @spawn generate_risk_dataset(proc_flags)
        push!(futures, f)
    end

    # wait for each process to terminate then collect the datasets
    [fetch(f) for f in futures]
    collect_datasets(dataset_filepaths, flags["output_filepath"])

    # add attributes from flags
    h5writeattr(flags["output_filepath"], "risk", 
        convert(Dict, flags))
end

#=
Description:
    - generate in parallel a risk dataset derived from the 
    ngsim dataset.

Args:
    - flags: cmd line settings, constants
=#
function parallel_generate_ngsim_dataset(flags)
    # set seed globally for AutomotiveDrivingModels
    srand(flags["random_seed"])

    # replace with ngsim values
    flags["sim_burn_in_time"] = flags["NGSIM_burn_in_time"]
    flags["max_num_vehicles"] = flags["NGSIM_max_num_vehicles"]

    # collect vehicle ids to sample
    td = load_trajdata(flags["NGSIM_i101_set"])
    veh_ids = threshold_trajdata_ids(td, flags["num_frames_threshold"])
    veh_ids = veh_ids[1:flags["NGSIM_num_traj"]]

    # partition into sets for each process
    each = Int(floor(length(veh_ids) / flags["num_proc"]))
    proc_veh_ids = Dict()
    for proc_id in 1:flags["num_proc"]
        s = (proc_id - 1) * each
        proc_veh_ids[proc_id] = veh_ids[s + 1: s + each]
    end
    r = length(veh_ids) % flags["num_proc"]
    append!(proc_veh_ids[flags["num_proc"]], veh_ids[end - r + 1:end])

    # spawn the individual processes, each of which 
    # write to their own hdf5 file
    add_entry!(flags, "veh_ids", [-1], Vector{Int64})
    dir = dirname(flags["output_filepath"])
    filename = basename(flags["output_filepath"])
    dataset_filepaths::Vector{String} = []
    futures::Vector{Future} = []
    for i in 1:flags["num_proc"]

        # format output filepath for this process
        output_filepath = string(
            dir, "/proc_$(i)_$(filename)")
        push!(dataset_filepaths, output_filepath)

        # set flag values specific to each proc
        proc_flags = deepcopy(flags)
        proc_flags["random_seed"] = i
        proc_flags["output_filepath"] = output_filepath
        proc_flags["num_traj"] = length(proc_veh_ids[i])
        proc_flags["veh_ids"] = proc_veh_ids[i]

        # spawn the process and maintain its future
        f = @spawn generate_risk_dataset(proc_flags)
        push!(futures, f)
    end

    # wait for each process to terminate then collect the datasets
    [fetch(f) for f in futures]
    collect_datasets(dataset_filepaths, flags["output_filepath"])

    # add attributes from flags
    h5writeattr(flags["output_filepath"], "risk", 
        convert(Dict, flags))
end