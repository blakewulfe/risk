
#=
Description:
    - Function for aggregating a set of datasets into a single 
        dataset. Each dataset must have "feature" and "target"
        datasets. Note that aggregating hdf5 files may not 
        be necessary since you can reference external datasets 
        from a single hdf5 dataset. This is just a convenience
        to only deal with a single file.

Args:
    - input_filepaths: filepaths to datasets to aggregate
    - output_filepath: filepath to save aggregate dataset
=#
function collect_datasets(input_filepaths::Vector{String},
        output_filepath::String)

    # compute aggregate size of dataset and feature and target 
    # size by iterating through the input files first
    num_features, num_targets = -1, -1
    num_samples = 0
    for (idx, filepath) in enumerate(input_filepaths)
        h5open(filepath, "r") do proc_file
            if idx == 1
                num_features = size(proc_file["features"], 1)
                num_targets = size(proc_file["targets"], 1)
            end

            num_proc_features, num_proc_samples = size(
                proc_file["features"])
            num_proc_targets = size(proc_file["targets"], 1)

            # check that feature and target dims match across sets
            if num_proc_features != num_features
                throw(ErrorException("not all proc datasets have 
                    the same target dim. This proc: $(num_proc_features)
                    previous procs: $(num_features). Filepath: $(filepath)"))
            end
            if num_proc_targets != num_targets
                throw(ErrorException("not all proc datasets have 
                    the same target dim. This proc: $(num_proc_targets)
                    previous procs: $(num_targets). Filepath: $(filepath)"))
            end

            num_samples += num_proc_samples
        end
    end

    # set up aggregate dataset containers
    h5file = h5open(output_filepath, "w")
    risk_dataset = g_create(h5file, "risk")
    feature_set = d_create(risk_dataset, "features", 
        datatype(Float64), dataspace(num_features, num_samples))
    target_set = d_create(risk_dataset, "targets", 
        datatype(Float64), dataspace(num_targets, num_samples))

    # collect across datasets
    sidx = 0
    for filepath in input_filepaths
        proc_file = h5open(filepath, "r")
        num_proc_samples = size(proc_file["features"], 2)
        eidx = sidx + num_proc_samples
        feature_set[:, sidx + 1:eidx] = read(proc_file["features"])
        target_set[:, sidx + 1:eidx] = read(proc_file["targets"])
        sidx += num_proc_samples
    end

    # close file 
    close(h5file)
end

#=
Description:
    - Given trajectory data, return the ids of vehicles in that 
        data that were present for at least threshold frames

Args:
    - trajdata: data to search for vehicle ids
    - threshold: number of frames a vehicle must be 
        present in for it to be valid

Returns:
    - the ids of valid vehicles
=#
function threshold_trajdata_ids(trajdata::Trajdata, threshold::Int64)
    # count occurences
    counts = Dict()
    for s in trajdata.states
        if !in(s.id, keys(counts))
            counts[s.id] = 0
        end
        counts[s.id] += 1
    end

    # threshold vehicles by occurence count
    valid_ids = [id for (id, count) in counts if count > threshold]
    return valid_ids
end
