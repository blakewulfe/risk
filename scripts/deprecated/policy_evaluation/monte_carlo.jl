
#=
Description:
    - Simulates a single scene.

Args:
    - scene: the scene to simulate
    - context: context within which to simulate the scene
    - models: models present in the scene from which to derive actions
    - roadway: roadway on which to simulate
    - trajdata: data container to store trajectories
    - rng: random number generator
    - simulation_time: seconds to simulate trajectory
    - frame_idx: frame at which to insert values into trajdata

Returns:
    - terminal frame index into trajdata
=#
function simulate_scene!(scene::Scene, context::IntegratedContinuous, 
        models::Dict{Int, DriverModel}, roadway::Roadway, trajdata::Trajdata,
        rng::MersenneTwister, simulation_time::Float64, frame_idx::Int = 0)
    state_idx = frame_idx * scene.n_vehicles
    actions = get_actions!(Array(DriveAction, length(scene)), scene, roadway, models)
    for t in 0:context.Δt:(simulation_time)
        lo = state_idx + 1
        for veh in scene
            state_idx += 1
            trajdata.states[state_idx] = TrajdataState(veh.def.id, veh.state)
        end
        hi = state_idx
        frame_idx += 1
        trajdata.frames[frame_idx] = TrajdataFrame(lo, hi, t)
        get_actions!(actions, scene, roadway, models)
        tick!(scene, roadway, actions, models)
    end
    return frame_idx
end

#=
Description:
    - Simulates a scene, tracking target values, and stops
        if there is a collision.

Args:
    - scene: the scene to simulate
    - context: context within which to simulate the scene
    - models: models present in the scene from which to derive actions
    - roadway: roadway on which to simulate
    - rec: scene record to use for target extraction
    - rng: random number generator
    - simulation_time: seconds to simulate trajectory
    - ego_veh_id: id of ego vehicle
    - targets: array where to place target values
=#
function simulate_scene!(scene::Scene, context::IntegratedContinuous, 
        models::Dict{Int, DriverModel}, roadway::Roadway, rec::SceneRecord,
        rng::MersenneTwister, simulation_time::Float64, ego_veh_id::Int, 
        targets::Vector{Float64})
    actions = get_actions!(Array(DriveAction, length(scene)), 
        scene, roadway, models)
    empty!(rec)
    for t in 0:context.Δt:(simulation_time)
        get_actions!(actions, scene, roadway, models)
        tick!(scene, roadway, actions, models)
        update!(rec, scene)
        vehicle_index = get_index_of_first_vehicle_with_id(scene, ego_veh_id)
        extract_risk_targets!(targets, rec, roadway, vehicle_index)
        if targets[1] == 1.
            break
        end
    end
end

#=
Description:
    - Repeatedly simulate the provided scene, collecting 
        aggregate statistics about it.

Args:
    - scene: scene to simulate
    - context: context used for propagagting state
    - models: dict of models in the scene
    - roadway: roadway of scene
    - trajdata: data container used as temporary storage for scene
    - features: container to fill with features
    - targets: container to fill with target values
    - aggregate_targets: container used to collect targets
        across runs
    - rec: record used for computing features, targets
    - rng: generating random numbers
    - flags: options object
=#
function monte_carlo!(scene::Scene, context::IntegratedContinuous, 
        models::Dict{Int, DriverModel}, roadway::Roadway, 
        trajdata::Trajdata, ego_veh_id::Int64, features::Vector{Float64}, 
        targets::Vector{Float64}, aggregate_targets::Vector{Float64}, 
        rec::SceneRecord, rng::MersenneTwister, flags::Flags)

    # initialize trajdata and aggregate targets
    for veh in scene
        trajdata.vehdefs[veh.def.id] = veh.def
    end
    fill!(aggregate_targets, 0)

    # simulate the scene for an initial period
    frame_idx = simulate_scene!(scene, context, models, 
        roadway, trajdata, rng, flags["sim_burn_in_time"])

    # repeatedly simulate the scene starting from frame_idx
    for idx in 1:flags["num_monte_carlo_runs"]
        get!(scene, trajdata, frame_idx)
        simulate_scene!(scene, context, models, roadway, 
            rec, rng, flags["sampling_time"], ego_veh_id, targets)
        aggregate_targets[:] += targets
    end

    # extract features from the beginning of the scene
    extract_risk_features!(features, rec, scene, trajdata, 
        models, frame_idx, ego_veh_id)

    # divide by num_monte_carlo_runs to get average values
    aggregate_targets[:] /= flags["num_monte_carlo_runs"]
end
