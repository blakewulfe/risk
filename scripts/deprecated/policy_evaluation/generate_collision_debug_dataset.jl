#=
Todo:
1. visualize the scene to make sure it is correct
2. write tests for each function to make sure it works
3. go through and make sure you understand everything about this system
=#

using AutomotiveDrivingModels
using HDF5
using JLD

push!(LOAD_PATH, ".")
include("dataset_extraction.jl")
push!(LOAD_PATH, "../")
include("../auto_utils.jl")

# simulation constants
const SAMPLING_FREQUENCY = 1. # hz
const SAMPLING_SECONDS = 3 # sec
const INIT_COLLISION_DIST = 3. # m

# roadway constants
const NUM_LANES = 1
const ROADWAY_LENGTH = 100. # m

# vehicle state constants
const EGO_SPEED = 2. # m/s
const OTHER_SPEED = 1. # m/s

# vehicle constants
const VEH_LENGTH = 1.
const VEH_WIDTH = 1.

# feature constants
const NUM_FEATURES = 1
const NUM_TARGETS = 1

# dataset constants
const OUTPUT_FILEPATH = "../../data/datasets/debug.jld"

# set random seed
srand(0)

#=
Description:
    - build a driver that maintains a constant longitudinal speed

Args:
    - context: context within which to build the driver

Returns:
    - model: the driver model
=#
function build_static_driver(context::IntegratedContinuous)
    # static longitudinal driver with accel 0
    mlon = StaticLongitudinalDriver(0.)

    # build driver model
    model = Tim2DDriver(context, mlon=mlon)

    return model
end

#=
Description:
    - reset the scene with a debug scene as follows:
    - main idea is to be able to deterministically say whether the cars will collide.
        - gives a dataset that is simple to check for correctness
        - and that should be easy for the network to learn
    - 2 vehicles
        - ego vehicle traveling at 2 m/s always behind other vehicle
        - other vehicle traveling at 1 m/s
    - time
        - data collection at 2hz
        - collect for 2 seconds (so 5 samples total account for the 
            t = 0 sample)
    - distance
        - first vehicle will deterministically travel 2m/s * 2s = 4m
        - second vehicle will deterministically travel 1m/s * 2s = 2m
        - so if they start within (d1 - d2 + vehlength) = 4m - 2m + 1m = 3m, then they will deterministically collide
        - generate initial distance uniformly at random between 1 and 3 meters
    - 1 lane straight highway

Args:
    - scene: the object to populate with the starting states 
    - context: not sure what this is
    - models: vehicles in scene
    - trajdata: trajectory data, included to reset vehicle defs
=#
function reset!(scene::Scene, context::IntegratedContinuous, 
        models::Dict{Int, DriverModel}, roadway::Roadway,
        ego_road_idx::RoadIndex, other_road_idx::RoadIndex,
        trajdata::Trajdata, rng::MersenneTwister)

    # populate with new scene
    empty!(scene)

    # vehicle states
    ego_vehstate = VehicleState(Frenet(ego_road_idx, roadway), roadway, EGO_SPEED)
    other_vehstate = VehicleState(Frenet(other_road_idx, roadway), roadway, OTHER_SPEED)

    # move ego vehicle to random location on road, not within some % of edge of roadway
    # buffer = .3
    # ego_x = ROADWAY_LENGTH * (buffer + rand(rng) * (1 - buffer))
    ego_x = 10.
    ego_vehstate = move_along(ego_vehstate, roadway, ego_x)

    # place other vehicle between 1 and 3 meters in front of ego vehicle
    # accounting for the length 
    rand_displacement = max(min(randn(rng), 1.5), -1.5)
    other_x = rand_displacement + INIT_COLLISION_DIST + ego_x + VEH_LENGTH
    other_vehstate = move_along(other_vehstate, roadway, other_x)

    # add vehicles to scene
    ego_vehdef = VehicleDef(0, AgentClass.CAR, VEH_LENGTH, VEH_WIDTH)
    push!(scene, Vehicle(ego_vehstate, ego_vehdef))
    other_vehdef = VehicleDef(1, AgentClass.CAR, VEH_LENGTH, VEH_WIDTH)
    push!(scene, Vehicle(other_vehstate, other_vehdef))

    # build driver models
    models[0] = build_static_driver(context)
    models[1] = build_static_driver(context)

    # reset trajectory data
    trajdata.vehdefs[0] = ego_vehdef
    trajdata.vehdefs[1] = other_vehdef
end

#=
Description:
    - simulates a single scene

Args:
    - scene: the scene to simulate
    - context: context within which to simulate the scene
    - models: models present in the scene from which to derive actions
    - roadway: roadway on which to simulate
    - trajdata: data container to store trajectories
=#
function simulate_scene!(scene::Scene, context::IntegratedContinuous, 
        models::Dict{Int, DriverModel}, roadway::Roadway, trajdata::Trajdata,
        rng::MersenneTwister)
    actions = get_actions!(Array(DriveAction, length(scene)), scene, roadway, models)
    frame_idx = 0
    state_idx = 0
    for t in 0:context.Δt:SAMPLING_SECONDS
        lo = state_idx + 1
        for veh in scene
            state_idx += 1
            trajdata.states[state_idx] = TrajdataState(veh.def.id, veh.state)
        end
        hi = state_idx
        frame_idx += 1
        trajdata.frames[frame_idx] = TrajdataFrame(lo, hi, t)
        get_actions!(actions, scene, roadway, models)
        tick!(scene, roadway, actions, models)
    end
end

#=
Description:
    - generate a debug dataset by running set of simulations
        and collecting feature=>target values from those simulations
=#
function generate_debug_dataset()
    # random seed
    rng = MersenneTwister(1)

    # roadway
    roadway = gen_straight_roadway(NUM_LANES, ROADWAY_LENGTH)
    
    # vehicles
    ego_road_idx = RoadIndex(proj(VecSE2(0.0,0.0,0.0), roadway))
    other_road_idx = RoadIndex(proj(VecSE2(0.0,0.0,0.0), roadway))
    
    # dataset constants
    num_runs = 1000
    num_veh = 2
    num_frames_per_run = Int(SAMPLING_SECONDS * SAMPLING_FREQUENCY) + 1
    num_states_total = Int(num_runs * num_frames_per_run * num_veh)
    # num_train_frames = Int(num_runs * (num_frames_per_run - 1) * num_veh)

    # objects used during simulation
    scene = Scene(num_veh)
    context = IntegratedContinuous(1 / SAMPLING_FREQUENCY, 1)
    models = Dict{Int, DriverModel}()

    # traj data to use for storing information about an episode
    trajdata = Trajdata(roadway,
        Dict{Int, VehicleDef}(),
        Array{TrajdataState}(num_states_total),
        Array{TrajdataFrame}(num_frames_per_run))

    # dataset containers
    features = Array{Float64}(NUM_FEATURES)
    rec = SceneRecord(num_runs, SAMPLING_FREQUENCY, num_veh)
    # featureset are the features to save
    featureset = Array{Float64}(NUM_FEATURES, num_runs)
    # targetset are the taget values associated with those features
    targetset = Array{Float64}(NUM_TARGETS, num_runs)

    # repeatedly simulate, collecting dataset
    sample_idx = 1
    for idx in 1:num_runs
        reset!(scene, context, models, roadway, ego_road_idx, 
            other_road_idx, trajdata, rng)
        simulate_scene!(scene, context, models, roadway, trajdata, rng)
        sample_idx = extract_debug_features_targets!(trajdata,
            scene, rec, features, featureset, targetset, sample_idx)
    end

    # save to file
    JLD.save(OUTPUT_FILEPATH, "features", featureset, "targets", targetset)
end

function analyze_debug_dataset()
    dataset = JLD.load(OUTPUT_FILEPATH)
    features = dataset["features"]
    targets = dataset["targets"]

    println("size of dataset features: $(size(features))")
    println("size of dataset targets: $(size(targets))")
    println("fraction of collision: $(sum(targets) / length(targets))")
end

@time generate_debug_dataset()
analyze_debug_dataset()
