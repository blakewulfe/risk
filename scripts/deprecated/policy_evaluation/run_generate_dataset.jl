
using AutomotiveDrivingModels
@everywhere using NGSIM
@everywhere using HDF5

push!(LOAD_PATH, "../")
@everywhere include("../flags.jl")
@everywhere include("../auto_utils.jl")

push!(LOAD_PATH, ".")
@everywhere include("monte_carlo.jl")
@everywhere include("dataset_extraction.jl")
@everywhere include("generate_risk_dataset.jl")
@everywhere include("data_utils.jl")

push!(LOAD_PATH, "../scene_generation")
@everywhere include("../scene_generation/reset_scene.jl")

function analyze_risk_dataset(flags)
    dataset = h5open(flags["output_filepath"])
    features = read(dataset["risk/features"])
    targets = read(dataset["risk/targets"])
    println("avg features: $(mean(features, 2))")
    println("avg targets: $(mean(targets, 2))")
    println("size of dataset features: $(size(features))")
    println("size of dataset targets: $(size(targets))")
    println("fraction of collision: $(sum(targets) / length(targets))")
end

parse_flags!(FLAGS, ARGS)

# manually set num_procs
FLAGS["num_proc"] = max(nprocs() - 1, 1)
println("Generating dataset with the following settings")
println(FLAGS)

if FLAGS["dataset_type"] == "heuristic"
    @time parallel_generate_heuristic_dataset(FLAGS)
elseif FLAGS["dataset_type"] == "NGSIM"
    @time parallel_generate_ngsim_dataset(FLAGS)
end

@time analyze_risk_dataset(FLAGS)