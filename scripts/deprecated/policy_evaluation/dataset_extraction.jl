#=
Notes:
1. trajdata frames are stored with the last frame last, so get!(scene, trajdata, 1) gets the first frame and get!(scene, trajdata, last_frame_idx) gets the last frame added.
2. scene records are the opposite in the sense that in calling update! on a scene record, you want to do so in the order the scenes occurred so that the most recent scene is stored first in the record.
    - scene records must also have multiple frames loaded into them to compute
        certain features (e.g., accel and velocity)
=#

##################### Target Extraction #####################

#=
Description:
    - Derive target values from a sequence of frames in trajdata.

Args:
    - targets: where to place targets
    - rec: record to use in extraction
    - scene: scene to use in extraction
    - trajdata: data of simulation 
    - start_frame_idx: frame where monte carlo simulations start
    - veh_id: id of vehicle for which to extract targets
=#
function extract_risk_targets!(targets::Vector{Float64}, rec::SceneRecord,
        scene::Scene, trajdata::Trajdata, start_frame_idx::Int, 
        veh_id::Int = 1)
    
    @assert(start_frame_idx > 0)
    fill!(targets, 0)
    empty!(rec)
    
    for frame_idx = start_frame_idx:length(trajdata.frames)
        get!(scene, trajdata, frame_idx)
        vehicle_index = get_index_of_first_vehicle_with_id(scene, veh_id)
        update!(rec, scene)

        # hard brake target
        accel = convert(Float64, get(ACC, rec, trajdata.roadway, vehicle_index))
        if accel < -4.
            targets[2] = 1.
        end

        # collision target
        in_collision = convert(Float64, get(
            IS_COLLIDING, rec, trajdata.roadway, vehicle_index))
        if in_collision == 1.
            targets[1] = 1.
        end
    end
end

function extract_risk_targets!(targets::Vector{Float64}, 
        rec::SceneRecord, roadway::Roadway, vehicle_index::Int64)
    fill!(targets, 0)
    # hard brake target
    accel = convert(Float64, get(ACC, rec, roadway, vehicle_index))
    if accel < -4.
        targets[2] = 1.
    end
    # collision target
    in_collision = convert(Float64, get(
        IS_COLLIDING, rec, roadway, vehicle_index))
    if in_collision == 1.
        targets[1] = 1.
    end
end

##################### Feature Extraction #####################

function set_feature_missing!(features::Vector{Float64}, i::Int)
    features[i] = 0.0
    features[i+1] = 1.0
end

function set_feature!(features::Vector{Float64}, i::Int, v::Float64)
    features[i] = v
    features[i+1] = 0.0
end

function set_dual_feature!(features::Vector{Float64}, i::Int, f::FeatureValue)
    if f.i == FeatureState.MISSING
        set_feature_missing!(features, i)
    else
        set_feature!(features, i, f.v)
    end
end

function set_speed_and_distance!(features::Vector{Float64}, i::Int, 
        neigh::NeighborLongitudinalResult, scene::Scene)
    neigh.ind != 0 ? set_feature!(features, i, scene[neigh.ind].state.v) :
                      set_feature_missing!(features, i)
    neigh.ind != 0 ? set_feature!(features, i+2, neigh.Δs) :
                      set_feature_missing!(features, i+2)
end

#=
Description:
    - Derive feature values from a sequence of frames in trajdata.

Args:
    - features: where to place features
    - rec: record to use for feature extraction
    - scene: scene from which to extract features
    - trajdata: passed in for roadway
    - models: the driver models for the cars on the road
    - frame: index of the frame from which to extract features
    - veh_id: id of vehicle for which to extract features
=#
function extract_risk_features!(features::Vector{Float64}, rec::SceneRecord,
        scene::Scene, trajdata::Trajdata, models::Dict{Int, DriverModel},
        frame::Int64, veh_id::Int = 1)

    # set up the rec with previous and current frames
    get!(scene, trajdata, frame - 1)
    update!(rec, scene)
    get!(scene, trajdata, frame)
    update!(rec, scene)
    vehicle_index = get_index_of_first_vehicle_with_id(scene, veh_id)
    
    roadway = trajdata.roadway
    veh_ego = scene[vehicle_index]
    
    # extract scene features
    features[1] = veh_ego.state.posF.t
    features[2] = veh_ego.state.posF.ϕ
    features[3] = veh_ego.state.v
    features[4] = veh_ego.def.length
    features[5] = veh_ego.def.width
    features[6] = convert(Float64, get(ACC, rec, roadway, vehicle_index))
    features[7] = convert(Float64, get(JERK, rec, roadway, vehicle_index))
    features[8] = convert(Float64, get(TURNRATEG, rec, roadway, vehicle_index))
    features[9] = convert(Float64, get(ANGULARRATEG, rec, roadway, vehicle_index))
    features[10] = convert(Float64, get(TURNRATEF, rec, roadway, vehicle_index))
    features[11] = convert(Float64, get(ANGULARRATEF, rec, roadway, vehicle_index))
    features[12] = convert(Float64, get(LANECURVATURE, rec, roadway, vehicle_index))
    features[13] = convert(Float64, get(MARKERDIST_LEFT, rec, roadway, vehicle_index))
    features[14] = convert(Float64, get(MARKERDIST_RIGHT, rec, roadway, vehicle_index))
    features[15] = convert(Float64, features[13] < -1.0 || features[14] < -1.0)
    features[16] = convert(Float64, veh_ego.state.v < 0.0)
    set_dual_feature!(features, 17, get(LANEOFFSETLEFT, rec, roadway, vehicle_index))
    set_dual_feature!(features, 19, get(LANEOFFSETRIGHT, rec, roadway, vehicle_index))

    # neighbor features
    F = VehicleTargetPointFront()
    R = VehicleTargetPointRear()
    left_lane_exists = convert(Float64, get(
        N_LANE_LEFT, rec, roadway, vehicle_index)) > 0
    right_lane_exists = convert(Float64, get(
        N_LANE_RIGHT, rec, roadway, vehicle_index)) > 0
    fore_M = get_neighbor_fore_along_lane(
        scene, vehicle_index, roadway, F, R, F)
    fore_L = get_neighbor_fore_along_left_lane(
        scene, vehicle_index, roadway, F, R, F)
    fore_R = get_neighbor_fore_along_right_lane(
        scene, vehicle_index, roadway, F, R, F)
    rear_M = get_neighbor_rear_along_lane(
        scene, vehicle_index, roadway, R, F, R)
    rear_L = get_neighbor_rear_along_left_lane(
        scene, vehicle_index, roadway, R, F, R)
    rear_R = get_neighbor_rear_along_right_lane(
        scene, vehicle_index, roadway, R, F, R)
    set_speed_and_distance!(features, 21, fore_M, scene)
    set_speed_and_distance!(features, 25, fore_L, scene)
    set_speed_and_distance!(features, 29, fore_R, scene)
    set_speed_and_distance!(features, 33, rear_M, scene)
    set_speed_and_distance!(features, 37, rear_L, scene)
    set_speed_and_distance!(features, 41, rear_R, scene)

    # extract driver behavior features
    beh_ego = models[veh_id]
    try
        features[46] = beh_ego.mlon.σ
        features[47] = beh_ego.mlon.k_spd
        features[48] = beh_ego.mlon.T
        features[49] = beh_ego.mlon.s_min
        features[50] = beh_ego.mlon.a_max
        features[51] = beh_ego.mlon.d_cmf
        features[52] = beh_ego.mlon.v_des
        features[53] = beh_ego.mlat.σ
        features[54] = beh_ego.mlat.kp
        features[55] = beh_ego.mlat.kd
        features[56] = beh_ego.mlane.politeness
        features[57] = beh_ego.mlane.advantage_threshold
    catch e
        features[45:57] = 0
    end
end

##################### Debug Extraction #####################

#=
Description:
    - derive targets from a scene record

Args:
    - 
=#
function extract_targets_from_rec!(rec::SceneRecord, roadway::Roadway, vehicle_index::Int, pastframe::Int = 0)
    # println(rec)
    # println(rec.scenes)
    # println(vehicle_index)
    # readline()
    return convert(Float64, get(IS_COLLIDING, rec, roadway, vehicle_index, pastframe))
end

#=
Description:
    - derive features from a scene record

Args:
=#
function extract_features_from_rec!(features::Vector{Float64}, 
        rec::SceneRecord, roadway::Roadway, vehicle_index::Int,
        pastframe::Int = 0)

    # get most recently added scene 
    # scene = rec.scenes[1]
    # println(scene)
    # readline()
    
    features[1] = convert(Float64, get(DIST_FRONT, rec, roadway, vehicle_index, pastframe))
    return features
end

#=
Description:
    - Extract a set of features from the trajectory data 
        and store in provided containers. This is performed
        over the extent of each episode and for every vehicle.

Args:
    - trajdata: trajectory data used to derive features
    - features: temp array used to store features
    - rec: scene record used for extracting features
    - featureset: the overall set of features
    - targetset: the overall set of target values
    - sample_idx: index into the datasets

Returns:
    - sample_idx: next index into the datasets
=#
function extract_features_targets!(trajdata::Trajdata, scene::Scene, 
        rec::SceneRecord, features::Array{Float64}, 
        featureset::Array{Float64}, targetset::Array{Float64},
        sample_idx::Int)
    roadway = trajdata.roadway
    num_frames_per_run = length(trajdata.frames)
    
    # collect features for each vehicle in the trajdata
    for id in keys(trajdata.vehdefs)
        empty!(rec)

        # collect features for each frame in this scene
        for frame in 1:num_frames_per_run
            get!(scene, trajdata, frame)
            update!(rec, scene)
            vehicle_index = get_index_of_first_vehicle_with_id(rec, id)
            
            # add targets
            if frame > 1
                targets = extract_targets_from_rec!(rec, roadway,
                vehicle_index)
                targetset[:, sample_idx] = targets
            end

            # add features
            if frame < num_frames_per_run
                sample_idx += 1
                extract_features_from_rec!(features, rec, roadway,
                vehicle_index)
                featureset[:, sample_idx] = features
            end
        end
    end
    return sample_idx
end

#=
Description:
    - Extract features and store in provided containers. 
        In this case we are only concered with collisions
        in the final frame, and using features from the 
        penultimate frame to predict that collision. 
        Furthermore, we are only interested in collecting
        features from the ego vehicle.

Args:
    - trajdata: trajectory data used to derive features
    - features: temp array used to store features
    - rec: scene record used for extracting features
    - featureset: the overall set of features
    - targetset: the overall set of target values
    - sample_idx: index into the datasets

Returns:
    - sample_idx: next index into the datasets
=#
function extract_debug_features_targets!(trajdata::Trajdata,
        scene::Scene, rec::SceneRecord, features::Array{Float64}, 
        featureset::Array{Float64}, targetset::Array{Float64},
        sample_idx::Int)
    # unpack values
    roadway = trajdata.roadway
    num_frames_per_run = length(trajdata.frames)
    ego_idx = 1

    # collect features for ego vehicle from penultimate frame
    empty!(rec)
    frame = num_frames_per_run - 1
    get!(scene, trajdata, frame)
    update!(rec, scene)
    extract_features_from_rec!(features, rec, roadway, ego_idx)
    featureset[:, sample_idx] = features

    # collect collision target from last frame
    empty!(rec)
    frame = num_frames_per_run
    get!(scene, trajdata, frame)
    update!(rec, scene)
    targets = extract_targets_from_rec!(rec, roadway, ego_idx)
    targetset[:, sample_idx] = targets

    # increment and return sample idx
    sample_idx += 1
    return sample_idx
end