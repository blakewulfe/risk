
immutable IDMParam
    δ::Float64 # acceleration exponent [-]
    T::Float64 # desired time headway [s]
    v_des::Float64 # desired speed [m/s]
    s_min::Float64 # minimum acceptable gap [m]
    a_max::Float64 # maximum acceleration ability [m/s²]
    d_cmf::Float64 # comfortable deceleration [m/s²] (positive)
end

immutable MOBILParam
    politeness::Float64 # politeness factor
    safe_decel::Float64 # safe braking value
    advantage_threshold::Float64 # minimum accel
end

immutable Behavior
    idm_params::IDMParams
    mobil_params::MOBILParams
end