
#=
Description:
    - build a driver that maintains a constant longitudinal speed

Args:
    - context: context within which to build the driver

Returns:
    - model: the driver model
=#
function build_static_driver(context::IntegratedContinuous)
    # static longitudinal driver with accel 0
    mlon = StaticLongitudinalDriver(0.)

    # build driver model
    model = Tim2DDriver(context, mlon=mlon)

    return model
end

#=
Description:
    - Build an aggressive  driver.

Args:
    - context: context within which to build the driver
    - num_vehicles: number of other vehicles in scene
    - rng: random number generator
    - flags: constants to use

Returns:
    - model: the driver model
=#
function build_aggressive_driver(context::IntegratedContinuous, 
        num_vehicles::Int64, rng::MersenneTwister, flags::Flags)
    mlon = IntelligentDriverModel(
        σ=0.1,
        k_spd=1.5,
        T=0.5,
        s_min=4.0,
        a_max=4.0,
        d_cmf=2.5,
        )
    σ=0.1
    mlat = ProportionalLaneTracker(σ=0.1, kp=3.5, kd=2.5)
    mlane = MOBIL(context,
                  politeness=0.1,
                  advantage_threshold=0.01)
    model = Tim2DDriver(context, 
        rec=SceneRecord(1, context.Δt, num_vehicles), 
        mlat=mlat, mlon=mlon, mlane=mlane)
    desired_speed = (flags["desired_speed"] + 
        randn(rng) * flags["desired_speed_variance"])
    set_desired_speed!(model, desired_speed)
    return model
end

#=
Description:
    - Build an aggressive  driver.

Args:
    - context: context within which to build the driver
    - num_vehicles: number of other vehicles in scene
    - rng: random number generator
    - flags: constants to use

Returns:
    - model: the driver model
=#
function build_driver(context::IntegratedContinuous, 
        num_vehicles::Int64, rng::MersenneTwister, flags::Flags)
    mlon = IntelligentDriverModel(
        k_spd=abs(1.25 + randn(rng) * .1),
        T=abs(1.125 + randn(rng) * .1),
        s_min=abs(4.5 + randn(rng) * .1),
        a_max=abs(3.0 + randn(rng) * .1),
        d_cmf=abs(1.75 + randn(rng) * .1),
        d_max=abs(2.0 + randn(rng) * .1)
        )
    mlon.σ = abs(0.1 + randn(rng) * .1)

    mlat = ProportionalLaneTracker(
        σ=abs(0.1 + randn(rng) * .1), 
        kp=abs(3.25 + randn(rng) * .1), 
        kd=abs(2.25 + randn(rng) * .1))

    mlane = MOBIL(context,
                  safe_decel=abs(2.0 + randn(rng) * .2),
                  politeness=abs(0.3 + randn(rng) * .2),
                  advantage_threshold=abs(.25 + randn(rng) * .2))
    model = Tim2DDriver(context, 
        rec=SceneRecord(1, context.Δt, num_vehicles), 
        mlat=mlat, mlon=mlon, mlane=mlane)
    desired_speed = (flags["desired_speed"] + 
        randn(rng) * flags["desired_speed_variance"])
    set_desired_speed!(model, desired_speed)
    return model
end

#=
Description
    - Generate initial road indices for vehicles.

Args:
    - roadway: roadway on which to place the vehicles
    - num_vehicles: number of vehicle road indexs generate
    - rng: random num generator
    - flags: constants, cmd line args

Returns:
    - vector of RoadIndex objects, one for each vehicle
=#
function generate_init_road_idxs(roadway::Roadway, num_vehicles::Int64, 
        rng::MersenneTwister, flags::Flags)
    init_road_idxs = Vector{RoadIndex}(num_vehicles)
    for idx in 1:num_vehicles
        lane_offset = -rand(rng, 0:(flags["num_lanes"] - 1)) * flags["lane_width"]
        road_idx = RoadIndex(proj(VecSE2(0.0, lane_offset, 0.0), roadway))
        init_road_idxs[idx] = road_idx
    end
    return init_road_idxs
end

#=
Description:
    - Check whether the proposed placement is valid. 
        This function assumes that none of the positions
        are greater than the length of the roadway.

Args:
    - pos: proposd roadway offset
    - positions: existing positions
    - lanes: list of lane values
    - flags: constants / options

Returns:
    - valid: whether the proposed position is valid
=#
function is_valid(pos::Float64, positions::Vector{Float64}, 
        lanes::Vector{Int64}, flags::Flags)
    valid = true
    cur_idx = length(positions) + 1
    for (idx, other_pos) in enumerate(positions)
        # skip if lanes differ
        if lanes[cur_idx] == lanes[idx]

            # need to account for wrap around, though
            # assume all the individual values are positive
            dist = abs(pos - other_pos)
            # e.g., if roadway_length = 10
            # pos1 = 1, pos2 = 9, dist = 8
            # then true_dist = 10 - 8 = 2
            dist = min(dist, flags["total_roadway_length"] - dist)
            if dist < flags["min_init_dist"]
                valid = false
                break
            end
        end
    end
    return valid
end

#=
Description:
    - Checks whether valid positions are likely to be generated.

Args:
    - lanes: list of lane values 
    - flags: constants to use

Returns:
    - boolean of whether valid positions are likely
=#
function permits_valid_positions(lanes::Vector{Int64}, flags::Flags)
    # check whether a valid set of positions is possible
    counts = Dict()
    for l in lanes
        if in(l, keys(counts))
            counts[l] += flags["min_init_dist"]
        else
            counts[l] = flags["min_init_dist"]
        end
    end
    for c in values(counts)
        if c > flags["total_roadway_length"] / 1.5
            return false
        end
    end
    return true
end

#=
Description:
    - Generate random positions for each vehicle. Note that 
        this function assumes that the number of requested
        positions will fit on the roadway and does not 
        validate this. 

Args:
    - lanes: vector of lanes 
    - rng: rng used for random numbers
    - flags: constants / cmd line arguments

Returns:
    - the positions to place the vehicles
=#
function generate_road_positions(lanes::Vector{Int64}, 
        rng::MersenneTwister, flags::Flags)
    # check if valid placement possible
    if !permits_valid_positions(lanes, flags)
        throw(ArgumentError(
            "Too many positions requested: \n$(lanes)\n$(flags)"))
    end

    # select random position on roadway for ego vehicle
    ego_x = flags["total_roadway_length"] * rand(rng)

    # randomly generate locations for other vehicles around ego vehicle
    positions = Vector{Float64}()
    push!(positions, ego_x)
    dist_variance = flags["init_dist_variance"]
    for idx in 2:length(lanes)
        valid = false
        while !valid
            # place this car in front or behind
            sign = rand(rng) > .5 ? 1 : -1

            # select distance
            dist = flags["min_init_dist"] + rand(rng) * dist_variance

            # compute position and check if valid
            pos = (sign * dist + ego_x) % flags["total_roadway_length"]
            # if negative position then wrap around
            if pos < 0
                pos = flags["total_roadway_length"] + pos
            end
            valid = is_valid(pos, positions, lanes, flags)

            # if invalid increase variance in placement to avoid
            # infinite loop in many car scenario
            if !valid
                dist_variance += 1
            else
                push!(positions, pos)
            end
        end
    end
    return positions
end

#=
Description:
    - Reset the scene with new cars. 
Args:
    - scene: the object to populate with the starting states 
    - context: not sure what this is
    - models: vehicles in scene
    - roadway: roadway in which to place scene
    - rng: random number generator for reproducible datasets
    - flags: options / constants container

Side Effects:
    - empties scene and adds new vehicles
=#
function reset!(scene::Scene, context::IntegratedContinuous, 
        models::Dict{Int, DriverModel}, roadway::Roadway,
        rng::MersenneTwister, flags::Flags)

    # remove old contents of scene and models
    empty!(scene)
    empty!(models)

    # get initial road indices, positions, vehicles
    num_vehicles = rand(rng, 4:flags["max_num_vehicles"])
    init_road_idxs = generate_init_road_idxs(roadway, num_vehicles, rng, flags)
    lanes = [road_idx.tag.lane for road_idx in init_road_idxs]
    road_positions = generate_road_positions(lanes, rng, flags)
    
    # add vehicles to scene
    for (idx, (road_idx, road_pos)) in enumerate(
            zip(init_road_idxs, road_positions))

        # build and move vehicle state
        veh_state = VehicleState(Frenet(road_idx, roadway), 
            roadway, flags["base_speed"] + randn(rng) * flags["base_speed_variance"])
        veh_state = move_along(veh_state, roadway, road_pos)

        # build vehicle definition
        veh_def = VehicleDef(idx, AgentClass.CAR, 
            flags["vehicle_length"], flags["vehicle_width"])
        
        push!(scene, Vehicle(veh_state, veh_def))
        models[idx] = build_driver(context, num_vehicles, rng, flags)
    end
end

#=
Description:
    - Reset the given scene with the first scene from a 
        real trajectory dataset where an ego vehicle with 
        the given id is present.

Args:
    - scene: scene to reset
    - context: context for driver model
    - models: dictionary to populate with driver models
    - real_trajdata: trajdata of a real dataset
    - rng: rng to use for generating driver models
    - flags: settings to use for driver models
=#
function reset!(scene::Scene, context::IntegratedContinuous, 
        models::Dict{Int, DriverModel}, real_trajdata::Trajdata, 
        ego_veh_id::Int64, rng::MersenneTwister, flags::Flags)

    # remove old contents of scene and models
    empty!(scene)
    empty!(models)

    # get the first scene
    frame_idx = get_first_frame_with_id(real_trajdata, ego_veh_id)
    get!(scene, real_trajdata, frame_idx)
    
    # generate driver models for the scene
    num_vehicles = length(scene.vehicles)
    for veh in scene.vehicles 
        models[veh.def.id] = build_driver(
            context, num_vehicles, rng, flags)
    end
end