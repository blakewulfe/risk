
TEST_FLAGS = Flags()

# scene constants
# roadway
add_entry!(TEST_FLAGS, 
    "num_lanes", 2, Int64, 
    "Number of lanes in the simulations.")
add_entry!(TEST_FLAGS, 
    "roadway_length", 200., Float64, 
    "Length of the roadway.")
add_entry!(TEST_FLAGS, 
    "roadway_radius", 25., Float64, 
    "Radius of turns in the roadway.")
add_entry!(TEST_FLAGS, 
    "total_roadway_length", 557., Float64, 
    "Total length of the roadway.")
add_entry!(TEST_FLAGS, 
    "lane_width", 3., Float64, 
    "Width of lane in meters.")

# vehicles
add_entry!(TEST_FLAGS, 
    "max_num_vehicles", 20, Int64, 
    "Number of vehicles on the road.")
add_entry!(TEST_FLAGS, 
    "base_speed", 30., Float64, 
    "Base vehicle speed.")
add_entry!(TEST_FLAGS, 
    "base_speed_variance", 3., Float64, 
    "Variance factor in inital speed.")
add_entry!(TEST_FLAGS, 
    "desired_speed", 30., Float64, 
    "Desired vehicle speed.")
add_entry!(TEST_FLAGS, 
    "desired_speed_variance", 5., Float64, 
    "Variance factor in desired speed.")
add_entry!(TEST_FLAGS, 
    "vehicle_length", 5., Float64, 
    "Vehicle length.")
add_entry!(TEST_FLAGS, 
    "vehicle_width", 2., Float64, 
    "Vehicle width.")
add_entry!(TEST_FLAGS, 
    "min_init_dist", 8., Float64, 
    "Minimum distance between vehicles at start of simulation.")
add_entry!(TEST_FLAGS, 
    "init_dist_variance", 1., Float64, 
    "Base variance in initial distance of vehicles.")

# simulation constants
add_entry!(TEST_FLAGS, 
    "num_traj", 100, Int64, 
    "Number of unique trajectories.")
add_entry!(TEST_FLAGS, 
    "num_monte_carlo_runs", 50, Int64, 
    "Number of monte carlo runs per trajectory.")
add_entry!(TEST_FLAGS, 
    "sampling_freq", 10, Int64, 
    "Number of samples per second (i.e., hz).")
add_entry!(TEST_FLAGS, 
    "sampling_time", 2., Float64, 
    "Seconds to simulate trajectory.")
add_entry!(TEST_FLAGS, 
    "sim_burn_in_time", 3., Float64, 
    "Number of unique trajectories.")
add_entry!(TEST_FLAGS, 
    "random_seed", 1, Int64, 
    "Random seed value.")

# dataset constants
add_entry!(TEST_FLAGS, 
    "num_features", 28, Int64, 
    "Number of features (e.g., dist to car in front).")
add_entry!(TEST_FLAGS, 
    "num_targets", 1, Int64, 
    "Number of target values (e.g., p(collision).")