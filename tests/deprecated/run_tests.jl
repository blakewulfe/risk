
# includes
push!(LOAD_PATH, "../scripts/")
include("../scripts/auto_utils.jl")
include("../scripts/flags.jl")

push!(LOAD_PATH, "../scripts/neural_networks")
include("../scripts/neural_networks/neural_network.jl")

push!(LOAD_PATH, ".")
include("testing_flags.jl")
include("testing_utils.jl")

push!(LOAD_PATH, "../scripts/policy_evaluation")
include("../scripts/policy_evaluation/dataset_extraction.jl")
include("../scripts/policy_evaluation/monte_carlo.jl")

push!(LOAD_PATH, "../scripts/scene_generation")
include("../scripts/scene_generation/reset_scene.jl")

# tests
println("\ntest_neural_networks/test_neural_network.jl")
include("test_neural_networks/test_neural_network.jl")

println("\ntest_scene_generation/test_reset_scene.jl")
include("test_scene_generation/test_reset_scene.jl")

println("\ntest_policy_evaluation/test_dataset_extraction.jl")
include("test_policy_evaluation/test_dataset_extraction.jl")

println("\nAll tests pass!")