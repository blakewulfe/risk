
using AutomotiveDrivingModels
using Base.Test

function test_generate_init_road_idxs()
    parse_flags!(TEST_FLAGS, [])
    flags = TEST_FLAGS
    rng = MersenneTwister(flags["random_seed"])
    roadway = gen_stadium_roadway(flags["num_lanes"], 
        length = flags["roadway_length"],
        radius = flags["roadway_radius"])
    num_vehicles = 2
    actual_init = generate_init_road_idxs(roadway, num_vehicles, rng, flags)
    @test length(actual_init) == num_vehicles
end

function test_is_valid()
    parse_flags!(TEST_FLAGS, [])
    flags = TEST_FLAGS
    flags["min_init_dist"] = 10.

    rng = MersenneTwister(flags["random_seed"])
    roadway = gen_stadium_roadway(flags["num_lanes"], 
        length = flags["roadway_length"],
        radius = flags["roadway_radius"])
    
    # basic, two car tests
    lanes = [1,1]
    pos = 10.
    positions = [pos]
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == false

    lanes = [1,2]
    pos = 10.
    positions = [pos]
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == true

    lanes = [1,1]
    positions = [pos + flags["min_init_dist"]]
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == true

    positions = [pos - flags["min_init_dist"]]
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == true

    positions = [pos - flags["min_init_dist"] + 1]
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == false

    lanes = [1,1]
    pos = flags["total_roadway_length"]
    positions = [flags["min_init_dist"] - 1]
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == false

    lanes = [1,1]
    pos = flags["total_roadway_length"]
    positions = [flags["min_init_dist"] + 1]
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == true

    # multicar tests
    lanes = [1,2,1,1,1,1,1]
    pos = 50.
    positions = [5., 0., 15., 25., 35., 45.]
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == false

    pos = 55.
    valid = is_valid(pos, positions, lanes, flags)
    @test valid == true
end

function test_generate_road_positions()
    parse_flags!(TEST_FLAGS, [])
    flags = TEST_FLAGS
    flags["roadway_length"] = 300.
    flags["min_init_dist"] = 10.

    rng = MersenneTwister(flags["random_seed"])
    roadway = gen_stadium_roadway(flags["num_lanes"], 
        length = flags["roadway_length"],
        radius = flags["roadway_radius"])
    lanes = [1,2,2]
    actual_positions = generate_road_positions(lanes, rng, flags)

    lanes = ones(Int64, 10)
    actual_positions = generate_road_positions(lanes, rng, flags)
    sort!(actual_positions)
    for (p1, p2) in zip(actual_positions, actual_positions[2:end])
        @test abs(p1 - p2) > flags["min_init_dist"]
    end

end

function test_reset!()
    parse_flags!(TEST_FLAGS, [])
    flags = TEST_FLAGS

    scene = Scene(flags["max_num_vehicles"])
    context = IntegratedContinuous(1 / flags["sampling_freq"], 1)
    models = Dict{Int, DriverModel}()
    roadway = gen_stadium_roadway(flags["num_lanes"], 
        length = flags["roadway_length"],
        radius = flags["roadway_radius"])
    rng = MersenneTwister(flags["random_seed"])
    reset!(scene, context, models, roadway, rng, flags)
end

@time test_generate_init_road_idxs()
@time test_is_valid()
@time test_generate_road_positions()
@time test_reset!()
