
using AutomotiveDrivingModels
using Base.Test

# push!(LOAD_PATH, "../../scripts/")
# include("../../scripts/auto_utils.jl")
# include("../../scripts/flags.jl")
# push!(LOAD_PATH, "../../scripts/policy_evaluation")
# include("../../scripts/policy_evaluation/monte_carlo.jl")
# include("../../scripts/policy_evaluation/dataset_extraction.jl")

function test_extract_risk_features!()
    sampling_time = 1.
    samples_per_second = .1
    num_vehicles = 1
    max_n_frames = Int(sampling_time / samples_per_second + 1)
    roadway = gen_stadium_roadway(3, length = 100., radius = 25.)
    max_n_frames = Int(sampling_time / samples_per_second + 1)
    rec = SceneRecord(max_n_frames, samples_per_second, num_vehicles)
    scene = Scene(num_vehicles)
    trajdata = Trajdata(roadway,
        Dict{Int, VehicleDef}(),
        Array{TrajdataState}(max_n_frames * num_vehicles),
        Array{TrajdataFrame}(max_n_frames))
    models = Dict{Int, DriverModel}()
    context = IntegratedContinuous(samples_per_second, 1)

    features = Vector{Float64}(57)
    accel = 5.
    mlon = StaticLongitudinalDriver(accel)
    models[1] = Tim2DDriver(context, mlon=mlon)
    
    road_idx = RoadIndex(proj(VecSE2(0.0, 4, 0.0), roadway))
    veh_state = VehicleState(Frenet(road_idx, roadway), roadway, 10.)
    veh_state = move_along(veh_state, roadway, 10.)
    veh_def = VehicleDef(1, AgentClass.CAR, 5., 2.)
    push!(scene, Vehicle(veh_state, veh_def))

    frame_idx = simulate_scene!(scene, context, models, roadway, 
        trajdata, MersenneTwister(1), sampling_time, 0)

    for veh in scene
        trajdata.vehdefs[veh.def.id] = veh.def
    end

    vehicle_index = 1

    extract_risk_features!(features, rec, scene, trajdata, models, frame_idx)
    @test features[6] == accel
end

function test_extract_risk_targets!()
    sampling_time = 1.
    sampling_freq = 10.
    num_vehicles = 2
    num_targets = 2

    max_n_frames = Int(sampling_time * sampling_freq + 1)
    roadway = gen_stadium_roadway(3, length = 100., radius = 25.)
    max_n_frames = Int(sampling_time * sampling_freq + 1)
    rec = SceneRecord(max_n_frames, 1. / sampling_freq, num_vehicles)
    scene = Scene(num_vehicles)
    targets = Vector{Float64}(num_targets)
    trajdata = Trajdata(roadway,
        Dict{Int, VehicleDef}(),
        Array{TrajdataState}(max_n_frames * num_vehicles),
        Array{TrajdataFrame}(max_n_frames))
    models = Dict{Int, DriverModel}()
    context = IntegratedContinuous(1. / sampling_freq, 1)

    # collision
    accel = 1.
    mlon = StaticLongitudinalDriver(accel)
    models[1] = Tim2DDriver(context, mlon=mlon)
    
    speed = 1.
    road_idx = RoadIndex(proj(VecSE2(0.0, 4, 0.0), roadway))
    veh_state = VehicleState(Frenet(road_idx, roadway), roadway, speed)
    veh_state = move_along(veh_state, roadway, 10.)
    veh_def = VehicleDef(1, AgentClass.CAR, 5., 2.)
    push!(scene, Vehicle(veh_state, veh_def))

    accel = 0.
    mlon = StaticLongitudinalDriver(accel)
    models[2] = Tim2DDriver(context, mlon=mlon)

    road_idx_2 = RoadIndex(proj(VecSE2(0.0, 4, 0.0), roadway))
    veh_state_2 = VehicleState(Frenet(road_idx_2, roadway), roadway, 10.)
    veh_state_2 = move_along(veh_state_2, roadway, 12.)
    veh_def_2 = VehicleDef(2, AgentClass.CAR, 5., 2.)
    push!(scene, Vehicle(veh_state_2, veh_def_2))

    frame_idx = simulate_scene!(scene, context, models, roadway, 
        trajdata, MersenneTwister(1), sampling_time)

    for veh in scene
        trajdata.vehdefs[veh.def.id] = veh.def
    end

    vehicle_index = 1

    extract_risk_targets!(targets, rec, scene, trajdata, 1)
    @test targets[1] == 1.

    # non collision
    empty!(scene)
    speed = 1.
    road_idx = RoadIndex(proj(VecSE2(0.0, 4, 0.0), roadway))
    veh_state = VehicleState(Frenet(road_idx, roadway), roadway, speed)
    veh_state = move_along(veh_state, roadway, 100.)
    veh_def = VehicleDef(1, AgentClass.CAR, 5., 2.)
    push!(scene, Vehicle(veh_state, veh_def))

    accel = 0.
    mlon = StaticLongitudinalDriver(accel)
    models[2] = Tim2DDriver(context, mlon=mlon)

    road_idx_2 = RoadIndex(proj(VecSE2(0.0, 4, 0.0), roadway))
    veh_state_2 = VehicleState(Frenet(road_idx_2, roadway), roadway, 10.)
    veh_state_2 = move_along(veh_state_2, roadway, 12.)
    veh_def_2 = VehicleDef(2, AgentClass.CAR, 5., 2.)
    push!(scene, Vehicle(veh_state_2, veh_def_2))

    simulate_scene!(scene, context, models, roadway, 
        trajdata, MersenneTwister(1), sampling_time, 0)

    extract_risk_targets!(targets, rec, scene, trajdata, 1)
    @test targets[1] == 0.

    # hard braking checks
    # no hard brake 
    trajdata = Trajdata(roadway,
        Dict{Int, VehicleDef}(),
        Array{TrajdataState}(max_n_frames),
        Array{TrajdataFrame}(max_n_frames))
    models = Dict{Int, DriverModel}()
    scene = Scene(1)
    speed = 1.
    road_idx = RoadIndex(proj(VecSE2(0.0, 4, 0.0), roadway))
    veh_state = VehicleState(Frenet(road_idx, roadway), roadway, speed)
    veh_state = move_along(veh_state, roadway, 10.)
    veh_def = VehicleDef(1, AgentClass.CAR, 5., 2.)
    push!(scene, Vehicle(veh_state, veh_def))

    for veh in scene
        trajdata.vehdefs[veh.def.id] = veh.def
    end

    accel = 200.
    mlon = StaticLongitudinalDriver(accel)
    models[1] = Tim2DDriver(context, mlon=mlon)
    simulate_scene!(scene, context, models, roadway, 
        trajdata, MersenneTwister(1), sampling_time, 0)

    extract_risk_targets!(targets, rec, scene, trajdata, 1)
    @test targets[1] == 0.
    @test targets[2] == 0.

    # with hard brake 
    empty!(scene)
    speed = 1.
    road_idx = RoadIndex(proj(VecSE2(0.0, 4, 0.0), roadway))
    veh_state = VehicleState(Frenet(road_idx, roadway), roadway, speed)
    veh_state = move_along(veh_state, roadway, 100.)
    veh_def = VehicleDef(1, AgentClass.CAR, 5., 2.)
    push!(scene, Vehicle(veh_state, veh_def))
    accel = -9.1
    mlon = StaticLongitudinalDriver(accel)
    models[1] = Tim2DDriver(context, mlon=mlon)
    simulate_scene!(scene, context, models, roadway, 
        trajdata, MersenneTwister(1), sampling_time, 0)

    extract_risk_targets!(targets, rec, scene, trajdata, 1)
    @test targets[1] == 0.
    @test targets[2] == 1.

end

@time test_extract_risk_features!()
@time test_extract_risk_targets!()
