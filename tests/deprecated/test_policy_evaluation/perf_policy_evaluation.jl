using AutomotiveDrivingModels

push!(LOAD_PATH, "../../scripts/")
include("../../scripts/auto_utils.jl")
include("../../scripts/flags.jl")

push!(LOAD_PATH, "../")
include("../testing_flags.jl")

push!(LOAD_PATH, "../../scripts/policy_evaluation")
include("../../scripts/policy_evaluation/monte_carlo.jl")
include("../../scripts/policy_evaluation/dataset_extraction.jl")

push!(LOAD_PATH, "../../scripts/scene_generation")
include("../../scripts/scene_generation/reset_scene.jl")

function test_simulate_scene()
    max_num_vehicles = 100
    sampling_freq = 10
    sampling_time = 60.
    num_lanes = 5

    parse_flags!(TEST_FLAGS, [])
    flags = TEST_FLAGS
    flags["max_num_vehicles"] = max_num_vehicles
    flags["num_lanes"] = num_lanes

    rng = MersenneTwister(1)

    roadway = gen_stadium_roadway(num_lanes, 
        length = 200.,
        radius = 50.)

    scene = Scene(max_num_vehicles)
    context = IntegratedContinuous(1 / sampling_freq, 1)
    models = Dict{Int, DriverModel}()

    num_frames_per_episode = Int(sampling_time * sampling_freq + 10) 
    num_states_total = Int(num_frames_per_episode * max_num_vehicles)

    trajdata = Trajdata(roadway,
        Dict{Int, VehicleDef}(),
        Array{TrajdataState}(num_states_total),
        Array{TrajdataFrame}(num_frames_per_episode))

    rec = SceneRecord(num_frames_per_episode, 
        1. / sampling_freq, max_num_vehicles)
    targets = Vector{Float64}(2)

    reset!(scene, context, models, roadway, rng, flags)

    num_runs = 1
    @time for i in 1:num_runs
        simulate_scene!(deepcopy(scene), context, models, roadway, 
            rec, rng, sampling_time, 1, targets)
        println(targets)
    end

    num_runs = 1
    @time for i in 1:num_runs
        simulate_scene!(deepcopy(scene), context, models, roadway, 
            trajdata, rng, sampling_time)
        println(targets)
    end

end

function main()
    test_simulate_scene()
end

main()